#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  1 16:19:11 2022

@author: konsfra
"""

from collections import Counter

import matplotlib.pyplot as plt
import numpy as np

sample = np.array(np.random.normal(100, 14, size=20000), dtype=int)
x_ = plt.hist(sample, bins=100)
count = Counter(sample)
rank = sorted(count.values(), reverse=True) 
plt.loglog(rank) 
phi = np.cumsum(rank)/sum(rank)
plt.plot(np.log(rank), phi)
